package com.trichashapps.wth.utils;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;

/**
 * Created by ashish on 3/5/17.
 */

public class ProgressDialog extends DialogFragment {

    private static final String ARGS_MESSAGE_ID = "message_id";

    public static ProgressDialog newInstance(int messageId) {
        ProgressDialog progressDialog = new ProgressDialog();
        Bundle args = new Bundle();
        args.putInt(ARGS_MESSAGE_ID, messageId);
        progressDialog.setArguments(args);
        return progressDialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int messageId = getArguments().getInt(ARGS_MESSAGE_ID);
        android.app.ProgressDialog dialog = new android.app.ProgressDialog(getActivity());
        dialog.setMessage(getString(messageId));
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        DialogInterface.OnKeyListener keyListener = new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                return keyCode == KeyEvent.KEYCODE_BACK;
            }
        };
        dialog.setOnKeyListener(keyListener);

        return dialog;
    }


    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            super.show(manager, tag);
        } catch (Exception e) {
            e.printStackTrace();
            //TODO see if this can fix this crash
        }
    }
}
