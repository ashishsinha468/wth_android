package com.trichashapps.wth.utils;

import android.util.Log;

import com.trichashapps.wth.application.Constants;

/**
 * Created by ashish on 3/5/17.
 */

public class Logger {
    private static final boolean isLoggerOn = true;
    private static final String TAG = Constants.LOGGER_TAG;

    public static void logError(String message) {
        if (!MiscUtils.isProduction())
            Log.e(TAG, message);
    }

    public static void logInfo(String message) {
        if (!MiscUtils.isProduction())
            Log.d(TAG, message);
    }
}
