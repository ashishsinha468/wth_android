package com.trichashapps.wth.ui.adapters.interfaces;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.List;

/**
 * Created by ashish on 3/5/17.
 */

public abstract class ViewHolder<T> extends RecyclerView.ViewHolder {
    public ViewHolder(View itemView) {
        super(itemView);
    }

    public abstract void bindView(Context context, View view);

    public abstract void bindData(List<T> dataList);
}
