package com.trichashapps.wth.ui.activities;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.widget.TextView;

import com.trichashapps.wth.BuildConfig;
import com.trichashapps.wth.R;
import com.trichashapps.wth.application.Constants;
import com.trichashapps.wth.model.domain.User;
import com.trichashapps.wth.utils.GsonUtils;
import com.trichashapps.wth.utils.SharedPreferencesUtil;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by ashish on 19/5/17.
 */

class BrandScreenActivity extends BaseActivity {
    @InjectView(R.id.cl_brand_screen)
    CoordinatorLayout clBrandScreen;

    @InjectView(R.id.tv_app_version)
    TextView tvAppVersion;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brand_screen);
        ButterKnife.inject(this);
        init();
    }

    private void init() {
        initAppVersion();
        initUser();
        delegateFlow();
    }

    private void delegateFlow() {
        if (user == null) {
            goToLoginActivity();
        } else {
            goToPassCodeActivity();
        }
    }

    private void goToPassCodeActivity() {
        startActivity(PassCodeActivity.newIntent(this));
    }

    private void goToLoginActivity() {
        startActivity(LoginActivity.newIntent(this));
    }

    private void initAppVersion() {
        tvAppVersion.setText(BuildConfig.VERSION_NAME);
    }

    private void initUser() {
        SharedPreferencesUtil sharedPreferencesUtil = SharedPreferencesUtil.getInstance(this);
        String userString = sharedPreferencesUtil.getData(Constants.BUNDLE_KEYS.user, null);
        user = GsonUtils.getObjectFromJson(userString, User.class);
    }
}
