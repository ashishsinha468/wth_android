package com.trichashapps.wth.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.trichashapps.wth.ui.adapters.interfaces.ViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ashish on 3/5/17.
 */

public abstract class RecyclerViewCustomAdapter<T> extends RecyclerView.Adapter<ViewHolder<T>> {
    private ViewHolder<T> viewHolder;
    List<T> dataList = new ArrayList<>();
    Context context;
    LayoutInflater layoutInflater;
    int layoutResourceId;

    public RecyclerViewCustomAdapter(ViewHolder<T> viewHolder, int layoutResourceId) {
        this.layoutResourceId = layoutResourceId;
        this.viewHolder = viewHolder;
    }

    @Override
    public ViewHolder<T> onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(layoutResourceId, parent, false);
        viewHolder.bindView(context, view);
        viewHolder.bindData(dataList);
        return viewHolder;
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}
