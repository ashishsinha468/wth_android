package com.trichashapps.wth.ui.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by ashish on 19/5/17.
 */

class PassCodeActivity extends BaseActivity {

    public static Intent newIntent(AppCompatActivity activity) {
        Intent intent = new Intent(activity, PassCodeActivity.class);
        return intent;
    }
}
