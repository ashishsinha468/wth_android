package com.trichashapps.wth.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.trichashapps.wth.R;
import com.trichashapps.wth.communication.presenters.LoginPresenter;

import butterknife.ButterKnife;

/**
 * Created by ashish on 19/5/17.
 */

class LoginActivity extends BaseActivity {
    private LoginPresenter presenter;

    public static Intent newIntent(AppCompatActivity activity) {
        Intent intent = new Intent(activity, LoginActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.inject(this);
        init();
    }

    private void init() {
        initPresenter();
    }

    private void initPresenter() {
        presenter=new LoginPresenter();
    }
}
