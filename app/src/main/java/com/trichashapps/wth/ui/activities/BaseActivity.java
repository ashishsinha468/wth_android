package com.trichashapps.wth.ui.activities;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.trichashapps.wth.R;
import com.trichashapps.wth.utils.ProgressDialog;

/**
 * Created by ashish on 3/5/17.
 */

public class BaseActivity extends AppCompatActivity {

    private static ProgressDialog progressDialog;

    public BaseActivity() {
        progressDialog = ProgressDialog.newInstance(R.string.dialog_progress_please_wait);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public synchronized void showProgress() {
        try {
            if (progressDialog != null && !progressDialog.isAdded()) {

                progressDialog.show(getFragmentManager(), null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public synchronized void hideProgress() {
        if (progressDialog != null) {
            try {
                progressDialog.dismissAllowingStateLoss();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    protected void toast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    protected void toastCustom(String message, int gravity, int duration) {
        Toast toast = Toast.makeText(this, message, duration);
        toast.setGravity(gravity, 0, 0);
        toast.show();
    }

    public void snack(View view, String message, int notificationType, int duration) {
        Snackbar snackbar = Snackbar.make(view, message, duration)
                .setAction("Action", null);
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(this, notificationType));
        snackbar.show();
    }

    public void snackWithCallback(View view, String message, int notificationType, int duration, String actionText, View.OnClickListener callback) {
        Snackbar snackbar = Snackbar.make(view, message, duration)
                .setAction(actionText, callback);
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(this, notificationType));
        snackbar.show();
    }
}

