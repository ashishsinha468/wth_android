package com.trichashapps.wth.ui.activities;

import android.content.Context;
import android.os.Bundle;

import com.trichashapps.wth.R;
import com.trichashapps.wth.application.App;
import com.trichashapps.wth.communication.presenters.interfaces.IHomeView;

public class HomeActivity extends BaseActivity implements IHomeView {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showMessage(String message, int color) {
        //Show Message whichever way you like
    }
}
