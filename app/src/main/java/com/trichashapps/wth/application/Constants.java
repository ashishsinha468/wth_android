package com.trichashapps.wth.application;

import com.trichashapps.wth.BuildConfig;

/**
 * Created by ashish on 3/5/17.
 */

public class Constants {
    public static final String LOGGER_TAG = BuildConfig.APPLICATION_ID;
    public static final String PREFERENCES_FILENAME = "";//TODO add a preference file name;
    public static final String PROJECT_NAME = "";//TODO add your project name

    public class BUNDLE_KEYS {
        public static final String AUTH_TOKEN = "authToken";
        public static final String user = "user";
    }
}
