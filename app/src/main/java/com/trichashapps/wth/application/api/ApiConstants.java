package com.trichashapps.wth.application.api;

/**
 * Created by ashish on 3/5/17.
 */

public class ApiConstants {
    public static final String PROD_URL = "";//TODO add a prod url;
    public static final String BASE_URL = "";//TODO add a base url
    public static final long CONNECTION_TIMEOUT = 30000;

    public static String USED_URL = "";//TODO add a used url

    public class REST_API {
        public static final String AUTH_HEADER_KEY = "auth-token";
        public static final String SOURCE_KEY = "source";//TODO add a source key
        public static final String VERSION_NAME = "versionName";
        public static final String TIMESTAMP = "timestamp";
    }
}
