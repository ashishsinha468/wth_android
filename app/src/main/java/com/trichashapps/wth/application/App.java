package com.trichashapps.wth.application;

import android.app.Application;
import android.content.Context;

import com.google.android.gms.analytics.Tracker;
import com.norbsoft.typefacehelper.TypefaceCollection;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.squareup.otto.Bus;
import com.trichashapps.wth.communication.EventManager;
import com.trichashapps.wth.communication.bus.BusProvider;

/**
 * Created by ashish on 3/5/17.
 */

public class App extends Application {
    private static Context context;
    private static TypefaceCollection typeface;
    private EventManager mManager;
    private Bus mBus = BusProvider.getInstance();

    public static Context getAppContext() {
        return App.context;
    }

    public static TypefaceCollection getTypeface() {
        return typeface;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        App.context = getApplicationContext();
        initTypeface();
        initManagerAndBus();
    }

    private void initManagerAndBus() {
        mManager = new EventManager(this, mBus);
        mBus.register(mManager);
        mBus.register(this);
    }

    private void initTypeface() {
        typeface = TypefaceCollection.createSystemDefault();
        TypefaceHelper.init(typeface);
    }
}
