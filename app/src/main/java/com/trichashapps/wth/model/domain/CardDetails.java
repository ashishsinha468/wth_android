package com.trichashapps.wth.model.domain;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ashish on 19/5/17.
 */

class CardDetails {
    @SerializedName("availableBalance")
    private String availableBalance;

    @SerializedName("currentOutstanding")
    private String currentOutstanding;

    @SerializedName("dueDate")
    private long timestamp;
}
