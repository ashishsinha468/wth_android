package com.trichashapps.wth.model.domain;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ashish on 19/5/17.
 */

class Transaction {

    @SerializedName("id")
    private String id;

    @SerializedName("referenceNumber")
    private String referenceNumber;

    @SerializedName("dateOfTransaction")
    private long transactionDate;

    @SerializedName("merchantName")
    private String merchantName;

    @SerializedName("referenceName")
    private String referenceName;

    @SerializedName("amount")
    private String amount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public long getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(long transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getReferenceName() {
        return referenceName;
    }

    public void setReferenceName(String referenceName) {
        this.referenceName = referenceName;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
