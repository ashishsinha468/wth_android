package com.trichashapps.wth.communication;

import android.content.Context;

import com.squareup.otto.Bus;

/**
 * Created by ashish on 3/5/17.
 */

public class EventManager {
    private Context mContext;
    private Bus mBus;
    private RestClient mRestClient;

    public EventManager(Context mContext, Bus mBus) {
        this.mContext = mContext;
        this.mBus = mBus;
        this.mRestClient = RestClient.getClient();
    }

    /**
     * listening to events
     */
}


