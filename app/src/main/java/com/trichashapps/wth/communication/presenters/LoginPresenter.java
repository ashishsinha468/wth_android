package com.trichashapps.wth.communication.presenters;

import com.squareup.otto.Bus;
import com.trichashapps.wth.communication.bus.BusProvider;
import com.trichashapps.wth.communication.presenters.interfaces.ILoginView;

/**
 * Created by ashish on 19/5/17.
 */

public class LoginPresenter implements BasePresenter<ILoginView> {
    ILoginView view;
    Bus mBus = BusProvider.getInstance();

    @Override
    public void attachView(ILoginView view) {
        this.view = view;
        mBus.register(this);
    }

    @Override
    public void detachView() {
        mBus.unregister(this);
        this.view = null;
    }
}
