package com.trichashapps.wth.communication.presenters.interfaces;

import android.content.Context;

/**
 * Created by ashish on 3/5/17.
 */

public interface BaseView {
    Context getContext();

    void showMessage(String message, int color);

    void showProgress();

    void hideProgress();

    //Add more view contracts if you need them
}
