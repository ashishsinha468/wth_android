package com.trichashapps.wth.communication.presenters;

/**
 * Created by ashish on 3/5/17.
 */

public interface BasePresenter<V> {
    public void attachView(V view);

    public void detachView();
}
