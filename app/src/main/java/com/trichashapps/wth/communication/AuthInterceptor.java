package com.trichashapps.wth.communication;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.trichashapps.wth.BuildConfig;
import com.trichashapps.wth.application.App;
import com.trichashapps.wth.application.Constants;
import com.trichashapps.wth.application.api.ApiConstants;
import com.trichashapps.wth.utils.Logger;
import com.trichashapps.wth.utils.SharedPreferencesUtil;

import java.io.IOException;


/**
 * Created by ashish on 3/5/17.
 */

public class AuthInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request originalRequest = chain.request();

        String authToken = SharedPreferencesUtil.getInstance(App.getAppContext()).getData(Constants.BUNDLE_KEYS.AUTH_TOKEN, "DUMMY");
        String source = Constants.PROJECT_NAME;
        String versionName = BuildConfig.VERSION_NAME;
        // Add authorization header with updated authorization value to intercepted request
        Request authorisedRequest = originalRequest.newBuilder()
                .header(ApiConstants.REST_API.AUTH_HEADER_KEY, authToken)
                .header(ApiConstants.REST_API.SOURCE_KEY, source)
                .header(ApiConstants.REST_API.VERSION_NAME, versionName)
                .header(ApiConstants.REST_API.TIMESTAMP, "" + System.currentTimeMillis())
                .build();

        Logger.logError("Sending request..." + authorisedRequest.toString());
        Logger.logError("with token, source..." + authToken + ", " + source);
        return chain.proceed(authorisedRequest);
    }
}
