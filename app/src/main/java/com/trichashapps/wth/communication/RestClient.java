package com.trichashapps.wth.communication;

import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;
import com.trichashapps.wth.application.api.ApiConstants;
import com.trichashapps.wth.utils.GsonUtils;
import com.trichashapps.wth.utils.MiscUtils;

import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

/**
 * Created by ashish on 3/5/17.
 */

public class RestClient {
    private static final String URL = ApiConstants.BASE_URL;

    private static RestClient mRestClient;
    private static RestAdapter mRestAdapter;
    Gson gson = GsonUtils.getDateCompatibleGson();


    private RestClient() {
        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(ApiConstants.CONNECTION_TIMEOUT, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(ApiConstants.CONNECTION_TIMEOUT, TimeUnit.SECONDS);

        okHttpClient.networkInterceptors().add(new AuthInterceptor());

        mRestAdapter = new RestAdapter.Builder()
                .setEndpoint(URL)
                .setClient(new OkClient(okHttpClient))
                .setConverter(new GsonConverter(gson))
                .setLogLevel(MiscUtils.isProduction() ? RestAdapter.LogLevel.NONE : RestAdapter.LogLevel.FULL)
                .build();
    }

    public static RestClient getClient() {
        if (mRestClient == null)
            mRestClient = new RestClient();
        return mRestClient;
    }

    /**
     * linking of interface to rest-client
     */


}
